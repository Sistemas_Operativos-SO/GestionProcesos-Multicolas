package vista.componentes.barraHerramientas;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import java.awt.Color;
import java.awt.Font;
import java.awt.Cursor;
import java.awt.Image;


public class BarraHerramientasTemplate extends JPanel {
    
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private JLabel lTItulo;
    private JButton bCerrar;
    
    //Objetos decoradores
    private Font fuenteTitulo;
    private ImageIcon iAux, iLogo, iCerrar;
    private Cursor cMano;

    public BarraHerramientasTemplate(BarraHerramientasComponent barraHerramientasComponent) {

        iLogo = new ImageIcon("");
        iCerrar = new ImageIcon("");

        iAux = new ImageIcon(iLogo.getImage().getScaledInstance(45, 45, Image.SCALE_AREA_AVERAGING));
        fuenteTitulo = new Font("Impact", Font.PLAIN, 24);
        lTItulo = new JLabel("MULTICOLAS");
        lTItulo.setFont(fuenteTitulo);
        lTItulo.setBounds(465, 0, 420 , 50);
        lTItulo.setForeground(Color.BLACK);
        lTItulo.setIcon(iAux);
        lTItulo.setHorizontalAlignment(SwingConstants.CENTER);
        this.add(lTItulo);
        cMano = new Cursor(Cursor.HAND_CURSOR);

        this.setSize(1350, 50);
        this.setLayout(null);
        this.addMouseMotionListener(barraHerramientasComponent);
        this.addMouseListener(barraHerramientasComponent);
        this.setBackground(new Color(237, 115, 115));
    }
}